package com.example.unicentro_v1_ms6

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.*
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.unicentro_v1_ms6.database.model.ProductModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore


class GroceryActivity : AppCompatActivity() {

    //declaration of Firestore database
    private lateinit var dbFirestore: FirebaseFirestore
    private lateinit var auth: FirebaseAuth

    private lateinit var listView: ListView

    private lateinit var textTotalPrice: TextView

    //loading progress circle
    private lateinit var progress: ProgressBar

    private lateinit var groceryList: MutableList<ProductModel>

    private lateinit var productAdapter:ProductAdapter
    private lateinit var productName: String



    var notConnected =true
    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false)
            if (notConnected) {
                Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            } else {
                Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            }
        }
    }

    @SuppressLint("LongLogTag", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grocery)
        groceryList= mutableListOf()
        dbFirestore = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        listView = findViewById(R.id.productList)
        textTotalPrice = findViewById(R.id.textTotalPrice)
        progress = findViewById(R.id.indeterminateBar)
        val dbRefPrice = dbFirestore.collection(auth.currentUser?.uid.toString()+1).document("total")
        loadGroceryList()

        dbRefPrice.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            if (snapshot != null && snapshot.exists()) {
                textTotalPrice.text="$ "+snapshot.data?.get("price").toString()
            } else {
            }
        }




        val fab = findViewById<FloatingActionButton>(R.id.floatIconId)
        fab.setOnClickListener {

            val intent = Intent(this, StoreActivity::class.java).apply {  }
            startActivity(intent)
        }
    }

    public override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }
    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    private fun loadGroceryList(){
        dbFirestore.collection(auth.currentUser?.uid.toString())
            .addSnapshotListener { value, e ->
                groceryList.clear()
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    Toast.makeText(baseContext,"Ocurio un error, intentalo mas tarde",Toast.LENGTH_LONG).show()
                    return@addSnapshotListener
                }
                for (product in value!!) {
                    var product= product.toObject(ProductModel::class.java)
                    groceryList.add(product)

                }

                productAdapter = ProductAdapter(applicationContext, R.layout.products, groceryList)
                productAdapter.groceryAdapter(this)
                listView.adapter = productAdapter
                productAdapter.total()
                progress.visibility=(View.GONE)

            }

    }
    fun setMarks(ref: DocumentReference){
        val dialog: AlertDialog =
            AlertDialog.Builder(this)
                .setTitle("Feedback")
                .setMessage("Califica según el costo/beneficio del producto.\n" +
                        "denuncia si consideras un abuso al consumidor")
                .setPositiveButton("Calificar", null) //Set to null. We override the onclick
                .setNegativeButton("Denunciar", null)
                .create()

        dialog.setOnShowListener {
            val buttonPositive: Button =dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            val buttonNegative: Button =dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
            buttonPositive.setOnClickListener(View.OnClickListener {
                rating(ref)
                dialog.dismiss()
            })
            buttonNegative.setOnClickListener(View.OnClickListener {
                report(ref)
                dialog.dismiss()
            })
        }
        dialog.show()
    }

    private fun report(ref: DocumentReference){
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS or InputType.TYPE_TEXT_FLAG_MULTI_LINE
        val dialog: AlertDialog =
            AlertDialog.Builder(this)
                .setTitle("Denuncia")
                .setMessage("Descargos: ")
                .setView(input)
                .setPositiveButton("Enviar", null) //Set to null. We override the onclick
                .setNegativeButton("Cancelar", null)
                .create()

        dialog.setOnShowListener {
            val button: Button =dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener(View.OnClickListener {
                ref.update("report", FieldValue.arrayUnion(input.text.toString()))
                    .addOnSuccessListener {
                    }
                    .addOnFailureListener{task->
                        Toast.makeText(baseContext,task.message,Toast.LENGTH_LONG).show()
                    }
                Toast.makeText(baseContext,"Gracias por enviarnos tu reporte.",Toast.LENGTH_LONG).show()
                dialog.dismiss()

            })
        }
        dialog.show()
    }
    private fun rating(ref: DocumentReference){
        val linearLayout = LinearLayout(this)
        val rating = RatingBar(this)
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        rating.layoutParams = lp
        rating.numStars = 5
        rating.stepSize = 1f
        linearLayout.addView(rating)
        val dialog: AlertDialog =
            AlertDialog.Builder(this)
                .setTitle("Puntuar")
                .setMessage("¿Qué te parece este producto?")
                .setView(linearLayout)
                .setPositiveButton("Enviar", null) //Set to null. We override the onclick
                .setNegativeButton("Cancelar", null)
                .create()

        dialog.setOnShowListener {
            val button: Button =dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener(View.OnClickListener {
                ref.update("rating", FieldValue.arrayUnion(rating.rating.toInt()))
                    .addOnSuccessListener {
                    }
                    .addOnFailureListener{task->
                        Toast.makeText(baseContext,task.message,Toast.LENGTH_LONG).show()
                    }
                Toast.makeText(baseContext,"Gracias por evaluar el producto.",Toast.LENGTH_LONG).show()
                dialog.dismiss()

            })
        }
        dialog.show()
    }

    companion object {

        private const val TAG = "ProductActivity"
        var productPrice = 0
        var productCate = ""

    }
}
