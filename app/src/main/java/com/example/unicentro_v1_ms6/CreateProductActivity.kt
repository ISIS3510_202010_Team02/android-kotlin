package com.example.unicentro_v1_ms6

import android.app.Activity
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.unicentro_v1_ms6.ConnectionReceiver.ConnectionReceiver
import com.example.unicentro_v1_ms6.database.model.ProductModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import okhttp3.OkHttpClient
import org.jetbrains.anko.doAsync
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory


class CreateProductActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, ConnectionReceiver.ConnectionReceiverListener {

    //firebase
    private lateinit var storage: StorageReference
    private lateinit var db: FirebaseFirestore

    //buttons
    private lateinit var buttomImage: Button
    private lateinit var fab: FloatingActionButton

    //product attributes
    private lateinit var name: EditText
    private lateinit var price: EditText
    private lateinit var category: String
    private lateinit var spinner:Spinner
    private lateinit var imageSelect: ImageView

    //image
    private var imageDownload: String = ""
    private  var img: Uri? =null

    //progress dialog
    private lateinit var dialog: AlertDialog


    var notConnected =false
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.reddit.com/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .client(OkHttpClient.Builder().build())
        .build()

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false)

            if (notConnected) {

                Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            } else {
                Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            }
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean){
        if(isConnected){

            Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
        }
    }

    public override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_product)

        //instanciate firestore
        db = FirebaseFirestore.getInstance()
        storage = FirebaseStorage.getInstance().getReference("images")

        //buttons
        buttomImage = findViewById(R.id.buttonImage)
        fab = findViewById(R.id.floatIconCreate)

        //product fields
        name = findViewById(R.id.CreateName)
        price = findViewById(R.id.CreatePrice)
        imageSelect = findViewById(R.id.imageSelect)
        spinner= findViewById(R.id.spinner)




        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.category_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
        spinner.onItemSelectedListener = this

        buttomImage.setOnClickListener {
            selectImageInAlbum()
        }


        fab.setOnClickListener {
            if(!notConnected){
                if(checkFields() ) {
                    progressDialog()
                    pushProduct()
                }
            }
            else{
                progressDialogFail()
                Toast.makeText(baseContext,"Revisa tu conexión", Toast.LENGTH_SHORT).show()
            }

        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        if (parent != null) {
            category=parent.getItemAtPosition(0).toString()
        }

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (parent != null) {
            category= parent.getItemAtPosition(position).toString()
        }
    }

    private fun progressDialog(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        val message = dialogView.findViewById<TextView>(R.id.message)
        message.text="creando Producto..."
        builder.setView(dialogView)
        builder.setCancelable(false)
        dialog= builder.create()
        dialog.show()
        doAsync {
            // do your background thread task
            while(true) {
                if (notConnected) {
                    dialog.dismiss()
                }
            }
        }
    }
    private fun progressDialogFail(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        val message = dialogView.findViewById<TextView>(R.id.message)
        message.text="No se pudo crear el producto, revisa tu conexión. Toca la pantalla para salir de este mensaje"
        builder.setView(dialogView)
        builder.setCancelable(true)
        dialog= builder.create()
        dialog.show()
    }
    private fun checkFields(): Boolean {
        when {
            TextUtils.isEmpty(name.text) -> {
                Toast.makeText(
                    baseContext,
                    "No tan rápido, Debes ponerle nombre al producto",
                    Toast.LENGTH_LONG
                ).show()
                return false
            }
            TextUtils.isEmpty(price.text) -> {
                Toast.makeText(
                    baseContext,
                    "No tan rápido, debes ponerle precio al producto",
                    Toast.LENGTH_LONG
                ).show()
                return false
            }
            img==null -> {
                Toast.makeText(
                    baseContext,
                    "No tan rápido, debes cargar la imagen del producto",
                    Toast.LENGTH_LONG
                ).show()
                return false
            }
            else -> return true
        }

    }

    private fun selectImageInAlbum() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, REQUEST_SELECT_IMAGE_IN_ALBUM)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SELECT_IMAGE_IN_ALBUM && resultCode == Activity.RESULT_OK) {
            val uri = data?.data
            if (uri != null) {
                img=uri
            }
            imageSelect.setImageURI(uri)
        }
    }

    private fun pushProduct(){

            val pathfile = img?.lastPathSegment?.let { storage.child(it) }
            pathfile?.putFile(img!!)?.addOnSuccessListener {
                pathfile.downloadUrl.addOnSuccessListener { uri ->
                    imageDownload = uri.toString()
                    addProductFirestore()
                }
            }?.addOnFailureListener {
                Toast.makeText(
                    baseContext,
                    "Falló la carga de imagen, intentalo mas tarde",
                    Toast.LENGTH_LONG
                ).show()
                dialog.dismiss()
            }
    }

    private fun addProductFirestore() {
        val replyIntent = Intent()
        val product = ProductModel(imageDownload, price.text.toString().toInt(), name.text.toString(),   "propia", category,0)
        db.collection("productos2")
            .add(product)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                Toast.makeText(
                    baseContext,
                    "Se creó un nuevo producto",
                    Toast.LENGTH_LONG
                ).show()
                dialog.dismiss()
                setResult(Activity.RESULT_OK, replyIntent)
                Handler().postDelayed({finish()},5000)
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
                Toast.makeText(
                    baseContext,
                    "No fue posible guardar el producto, intentalo mas tarde",
                    Toast.LENGTH_LONG
                ).show()
                dialog.dismiss()
            }

    }

    companion object {
        private const val REQUEST_SELECT_IMAGE_IN_ALBUM = 1
        private const val TAG = "CreateProductActivity"
    }


}