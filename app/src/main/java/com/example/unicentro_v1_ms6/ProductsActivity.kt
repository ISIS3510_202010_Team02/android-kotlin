    package com.example.unicentro_v1_ms6

    import android.annotation.SuppressLint
    import android.content.BroadcastReceiver
    import android.content.Context
    import android.content.Intent
    import android.content.IntentFilter
    import android.net.ConnectivityManager
    import android.os.Bundle
    import android.text.InputFilter
    import android.text.InputType
    import android.util.Log
    import android.view.View
    import android.widget.*
    import androidx.appcompat.app.AlertDialog
    import androidx.appcompat.app.AppCompatActivity
    import com.example.unicentro_v1_ms6.ConnectionReceiver.ConnectionReceiver
    import com.example.unicentro_v1_ms6.database.model.ProductModel
    import com.google.android.material.floatingactionbutton.FloatingActionButton
    import com.google.firebase.auth.FirebaseAuth
    import com.google.firebase.firestore.DocumentReference
    import com.google.firebase.firestore.FieldValue
    import com.google.firebase.firestore.FirebaseFirestore


    open class ProductsActivity : AppCompatActivity(),
        AdapterView.OnItemSelectedListener {


        //declaration of Firestore database
        private lateinit var dbFirestore: FirebaseFirestore
        private lateinit var auth: FirebaseAuth

        private lateinit var listView: ListView
        private lateinit var productList: MutableList<ProductModel>

        private lateinit var textName: TextView
        //loading progress circle
        private lateinit var progress: ProgressBar
        //spinners: help to filter the products for store and category.
        private lateinit var spinner: Spinner
        //filter store
        private lateinit var filterStore : String

        private lateinit var groceryList: MutableList<ProductModel>
        private lateinit var productName: String




        var notConnected =true
        private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                notConnected = intent.getBooleanExtra(
                    ConnectivityManager
                        .EXTRA_NO_CONNECTIVITY, false)
                if (notConnected) {
                    Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
                    MainActivity.not_connected = notConnected
                } else {
                    Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
                    MainActivity.not_connected = notConnected
                }
            }
        }


        @SuppressLint("LongLogTag")
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_products)
            productList = mutableListOf()
            groceryList= mutableListOf()
            dbFirestore = FirebaseFirestore.getInstance()
            auth = FirebaseAuth.getInstance()
            listView = findViewById(R.id.productList)
            textName = findViewById(R.id.textNameStore)
            progress = findViewById(R.id.indeterminateBar)
            spinner= findViewById(R.id.spinnerCategory)


            var catego = R.array.category_array
            //load products by store
            val parameters = this.intent.extras
            filterStore = "Productos"
            if (parameters != null) {
                //change store name
                val storeName = parameters.getString("store")
                textName.text = storeName
                if (textName.text=="Tiendas Ara")  {
                    catego=R.array.categoryAra_array
                }
                //load filtered products
                filterStore = parameters.getString("filterStore").toString()
            }
            loadGroceryList()
            loadProductsOneFilter(filterStore)

            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter.createFromResource(
                this,
                catego,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                spinner.adapter = adapter
            }
            spinner.onItemSelectedListener = this

            val fab = findViewById<FloatingActionButton>(R.id.floatIconId)
            fab.setOnClickListener {

                val intent = Intent(this, CreateProductActivity::class.java).apply {  }
                startActivity(intent)
            }

        }



        public override fun onStart() {
            super.onStart()
            registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        }
        override fun onStop() {
            super.onStop()
            unregisterReceiver(broadcastReceiver)
        }

        private fun loadGroceryList(){
            groceryList.clear()
            dbFirestore.collection(auth.currentUser?.uid.toString())
                .get().addOnSuccessListener { products->
                    for (product in products!!) {
                        val product = product.toObject(ProductModel::class.java)
                        groceryList.add(product)
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error getting documents: ", exception)
                    Toast.makeText(baseContext,"Ocurio un error, intentalo mas tarde",Toast.LENGTH_LONG).show()
                }
        }
        private fun loadProductsTwoFilters(storeFilter: String, CategoryFilter: String){
            productList.clear()
            //var tempList : MutableList<ProductModel>
            dbFirestore.collection("productos2")
                .whereEqualTo("category",CategoryFilter)
                .whereEqualTo("store", storeFilter)
                .get()
                .addOnSuccessListener { documents ->
                    for ((index,value) in documents.withIndex()) {
                        Log.d(TAG, "${value.id} => ${value.data}")
                        val product = value.toObject(ProductModel::class.java)
                        if(CategoryFilter=="promotion" && index%2==0){
                            continue
                        }
                        productList.add(product)

                        for(prod in groceryList){
                            if(prod.name==product.name){
                                product.amount=prod.amount
                                break
                            }

                        }
                        productPrice = product.price!!
                        productCate = product.category.toString()
                    }

                    val adapter = ProductAdapter(applicationContext, R.layout.products, productList)
                    adapter.yourArrayAdapter(this)
                    listView.adapter = adapter
                    progress.visibility=(View.GONE)
                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error getting documents: ", exception)
                    Toast.makeText(baseContext,"Ocurio un error, intentalo mas tarde",Toast.LENGTH_LONG).show()
                }
        }
        private fun loadProductsOneFilter(filter: String){
                productList.clear()
                dbFirestore.collection("productos2")
                    .whereEqualTo("store",filter)
                    .get()
                    .addOnSuccessListener { documents ->
                        for (document in documents) {
                            Log.d(TAG, "${document.id} => ${document.data}")
                            val product = document.toObject(ProductModel::class.java)
                            productList.add(product)
                            for(prod in groceryList){
                                if(prod.name==product.name){
                                    product.amount=prod.amount
                                    break
                                }
                            }
                            productPrice = product.price!!
                            productCate = product.category.toString()
                        }
                        val adapter = ProductAdapter(applicationContext, R.layout.products, productList)
                        adapter.yourArrayAdapter(this)
                        listView.adapter = adapter
                        progress.visibility=(View.GONE)
                    }
                    .addOnFailureListener { exception ->
                        Log.w(TAG, "Error getting documents: ", exception)
                        Toast.makeText(baseContext,"Ocurio un error, intentalo mas tarde",Toast.LENGTH_LONG).show()
                    }
        }

        fun setMarks(ref: DocumentReference){
            val dialog: AlertDialog =
                AlertDialog.Builder(this)
                    .setTitle("Feedback")
                    .setMessage("Califica según el costo/beneficio del producto.\n" +
                            "denuncia si consideras un abuso al consumidor")
                    .setPositiveButton("Calificar", null) //Set to null. We override the onclick
                    .setNegativeButton("Denunciar", null)
                    .create()

            dialog.setOnShowListener {
                val buttonPositive: Button =dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                val buttonNegative: Button =dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                buttonPositive.setOnClickListener(View.OnClickListener {
                    rating(ref)
                    dialog.dismiss()
                })
                buttonNegative.setOnClickListener(View.OnClickListener {
                    report(ref)
                    dialog.dismiss()
                })
            }
            dialog.show()
        }

        private fun report(ref: DocumentReference){
            val input = EditText(this)
            input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS or InputType.TYPE_TEXT_FLAG_MULTI_LINE
            val dialog: AlertDialog =
                AlertDialog.Builder(this)
                    .setTitle("Denuncia")
                    .setMessage("Descargos: ")
                    .setView(input)
                    .setPositiveButton("Enviar", null) //Set to null. We override the onclick
                    .setNegativeButton("Cancelar", null)
                    .create()

            dialog.setOnShowListener {
                val button: Button =dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                button.setOnClickListener(View.OnClickListener {
                    ref.update("report", FieldValue.arrayUnion(input.text.toString()))
                        .addOnSuccessListener {
                        }
                        .addOnFailureListener{task->
                            Toast.makeText(baseContext,task.message,Toast.LENGTH_LONG).show()
                        }
                    Toast.makeText(baseContext,"Gracias por enviarnos tu reporte.",Toast.LENGTH_LONG).show()
                    dialog.dismiss()

                })
            }
            dialog.show()
        }
        private fun rating(ref: DocumentReference){
            val linearLayout = LinearLayout(this)
            val rating = RatingBar(this)
            val lp = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            rating.layoutParams = lp
            rating.numStars = 5
            rating.stepSize = 1f
            linearLayout.addView(rating)
            val dialog: AlertDialog =
                AlertDialog.Builder(this)
                    .setTitle("Puntuar")
                    .setMessage("¿Qué te parece este producto?")
                    .setView(linearLayout)
                    .setPositiveButton("Enviar", null) //Set to null. We override the onclick
                    .setNegativeButton("Cancelar", null)
                    .create()

            dialog.setOnShowListener {
                val button: Button =dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                button.setOnClickListener(View.OnClickListener {
                    ref.update("rating", FieldValue.arrayUnion(rating.rating.toInt()))
                        .addOnSuccessListener {
                        }
                        .addOnFailureListener{task->
                            Toast.makeText(baseContext,task.message,Toast.LENGTH_LONG).show()
                        }
                    Toast.makeText(baseContext,"Gracias por evaluar el producto.",Toast.LENGTH_LONG).show()
                    dialog.dismiss()

                })
            }
            dialog.show()
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val category=parent?.getItemAtPosition(position).toString()
            loadGroceryList()
            if(category == "All"){
                loadProductsOneFilter(filterStore)
            }
            else{
                loadProductsTwoFilters(filterStore,parent?.getItemAtPosition(position).toString() )
            }

        }
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        companion object {

            private const val TAG = "ProductActivity"
            var productPrice = 0
            var productCate = ""

        }
    }
