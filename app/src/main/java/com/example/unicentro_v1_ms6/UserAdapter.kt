package com.example.unicentro_v1_ms6

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.unicentro_v1_ms6.database.model.UserModel
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class UserAdapter(val mCtx: Context, val layoutResId: Int, val userList: List<UserModel>):
    ArrayAdapter<UserModel>(mCtx,layoutResId,userList) {
        private var storage: StorageReference = FirebaseStorage.getInstance().reference


        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(layoutResId,null)
        val textViewName = view.findViewById<TextView>(R.id.nombreText)
        val textViewCedula = view.findViewById<TextView>(R.id.cedulaText)
            val textViewCity = view.findViewById<TextView>(R.id.ciudadText)
            val textViewUser = view.findViewById<TextView>(R.id.userText)
        val user = userList[position]
        textViewName.text=user.nombre
        textViewCedula.text=user.cedula
            textViewCity.text=user.ciudad
            textViewUser.text=user.usuario


        return  view
    }

}