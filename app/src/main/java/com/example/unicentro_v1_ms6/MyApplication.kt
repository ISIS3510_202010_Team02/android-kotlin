package com.example.unicentro_v1_ms6

import android.app.Application
import com.example.unicentro_v1_ms6.ConnectionReceiver.ConnectionReceiver

class MyApplication: Application(){
    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    fun setConnectionListener(listener: ConnectionReceiver.ConnectionReceiverListener){
        ConnectionReceiver.connectionReceiverListener = listener
    }

    companion object{
        @get:Synchronized
        lateinit var instance: MyApplication
    }
}