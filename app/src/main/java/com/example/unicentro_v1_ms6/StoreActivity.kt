package com.example.unicentro_v1_ms6

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.unicentro_v1_ms6.database.model.StoreModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot


class StoreActivity : AppCompatActivity() {

    private lateinit var listView: ListView
    lateinit var storeList: MutableList<StoreModel>

    private lateinit var progress: ProgressBar

    //firebase firestore
    private lateinit var dbFirestore: FirebaseFirestore

    var notConnected =true
    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false)
            if (notConnected) {
                Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            } else {
                Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store)

        storeList = mutableListOf()
        listView = findViewById(R.id.storeList)
        progress = findViewById(R.id.storeBar)
        dbFirestore = FirebaseFirestore.getInstance()

        dbFirestore.collection("stores")
            .addSnapshotListener(this
            ) { querySnapshot: QuerySnapshot?, e: FirebaseFirestoreException? ->
                if (e != null) {
                    Log.w(StoreActivity.TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }
                if (querySnapshot != null) {
                    storeList.clear()
                    for (document in querySnapshot.documents) {
                        val store = document.toObject(StoreModel::class.java)
                        storeList.add(store!!)
                        storeName = store.schedule.toString()

                    }

                    val adapter = StoreAdapter(applicationContext, R.layout.products, storeList)
                    listView.adapter = adapter
                    progress.visibility=(View.GONE);
                }
                else {
                    Toast.makeText(
                        baseContext,
                        "No hay tiendas",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        listView.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->

            val intent = Intent(this, ProductsActivity::class.java)
            intent.putExtra("store",storeList[position].name)
            intent.putExtra("filterStore", storeList[position].store)
            startActivity(intent)

            //Realiza alguna acción!
        })

    }

    public override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }
    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    companion object {

        private val TAG = "StoreActivity"
        var storeName = ""

    }

}