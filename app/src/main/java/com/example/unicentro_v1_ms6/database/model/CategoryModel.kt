package com.example.unicentro_v1_ms6.database.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class CategoryModel
    (var id: Int?, var name: String?, var description : String?, var image : String? )

