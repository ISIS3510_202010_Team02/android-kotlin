package com.example.unicentro_v1_ms6.database.model

import android.widget.ImageView
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class ProductModel
    (var img : String? ="", var price : Int? = 0, var name: String? = "", var store : String? ="", var category : String? = "", var amount : Int? = 0, var report : List<String>?= listOf<String>(), var rating :  List<Int>? = listOf<Int>())