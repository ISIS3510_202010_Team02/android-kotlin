package com.example.unicentro_v1_ms6.database.model

import com.google.firebase.database.IgnoreExtraProperties

    @IgnoreExtraProperties
    data class UserModel
        (var nombre: String? = "",var cedula : String? = "", var ciudad: String? = "",  var usuario : String? = "",  var nationality : String? = "",  var genre : String? = "",  var birth : String? = "")
