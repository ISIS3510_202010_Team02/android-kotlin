package com.example.unicentro_v1_ms6.database.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class StoreModel
    (var name: String? ="", var schedule : String? ="", var img : String? ="" , var store : String?="")

