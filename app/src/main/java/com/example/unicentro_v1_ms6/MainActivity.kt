package com.example.unicentro_v1_ms6

import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.unicentro_v1_ms6.ConnectionReceiver.ConnectionReceiver
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import com.newrelic.agent.android.NewRelic;
import com.newrelic.agent.android.logging.AgentLog


class MainActivity : AppCompatActivity() {


    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var userLocation: UserLocation
    private lateinit var firestore: FirebaseFirestore
    //private lateinit var mFusedLocationCliente: FusedLocationProviderClient
    private var price = ProductsActivity.productPrice
//    private val arrayList = ArrayList<String>()
//    private val adapter = RecyclerAdapter()
    private lateinit var dialog: AlertDialog
    var notConnected =true
    private val retrofit = Retrofit.Builder()
            .baseUrl("https://api.reddit.com/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .client(OkHttpClient.Builder().build())
            .build()

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
             notConnected = intent.getBooleanExtra(ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false)
            if (notConnected) {
                disconnected()
                not_connected = notConnected
            } else {
                connected()
                not_connected = notConnected
            }
        }
    }




    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        loginButton.setOnClickListener {
            doLogin()


        }

        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        /*val cm = baseContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo*/

        /*if(networkInfo != null && networkInfo.isConnected){
            //There is an internet connection
            if(networkInfo.type == ConnectivityManager.TYPE_WIFI)
            {
                Toast.makeText(baseContext,"You're connected via WiFi", Toast.LENGTH_SHORT).show()
            }
            if(networkInfo.type == ConnectivityManager.TYPE_MOBILE)
            {
                Toast.makeText(baseContext,"You're connected via Mobile", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(baseContext,"You have no internet connection", Toast.LENGTH_SHORT).show()
            }
        }*/

        logEvent()
        firestore = FirebaseFirestore.getInstance()

        NewRelic.withApplicationToken(
            "AA409ef6daef05116865c1e7c60a5a8223bd243334-NRMA"
        )
            .withLogLevel(AgentLog.AUDIT)
            .start(this.application)
    }

    /*private fun saveUserLocation(){
        if(userLocation != null){
            var locationRef = firestore.collection("userLocations").document(FirebaseAuth.getInstance().uid!!)
            locationRef.set(userLocation).addOnCompleteListener(this){task ->
                if(task.isSuccessful){
                    Log.d("succed", "task succesful for saving user location")
                }
            }
        }
    }*/

    /*private fun getLastKnownLocation(){

        mFusedLocationCliente.lastLocation.addOnCompleteListener(OnCompleteListener {
            fun onComplete(task: Task<android.location.Location>){
                var location = task.getResult()
                if (location != null) {
                    var geoPoint = GeoPoint(location.latitude, location.longitude)
                }
            }
        })

    }*/

    /*fun getUserDetails(){
        if(userLocation == null){
            userLocation = UserLocation()

            var userRef = firestore.collection("users").document(FirebaseAuth.getInstance().uid!!)
            userRef.get().addOnCompleteListener(){
                fun onComplete(task: Task<DocumentSnapshot>){
                    if(task.isSuccessful){
                        Log.d("success", "succesful get user details")
                        var user = task.getResult()
                        getLastKnownLocation()
                    }
                }
            }
        }
    }*/
    /** Called when the user taps the buttons */
    fun sendMenu(view: View) {

        val intent = Intent(this, UserMenuActivity::class.java).apply {
        }
        startActivity(intent)
    }

    fun sendRegistration(view: View) {
        val intent = Intent(this, UserRegistrationActivity::class.java).apply {
        }
        startActivity(intent)
    }

    companion object{
        var not_connected = true
    }

    fun progressDialog(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        val message = dialogView.findViewById<TextView>(R.id.message)
        message.text="Iniciando Sesión..."
        builder.setView(dialogView)
        builder.setCancelable(false)
        dialog= builder.create()
        dialog.show()
    }


    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUIAlready(currentUser)
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    private fun updateUIAlready(currentUser : FirebaseUser?){

        if (currentUser != null){
            if(currentUser.isEmailVerified) {
                var intent = Intent(this,UserMenuActivity::class.java)
                intent.putExtra("email", currentUser.email)
                startActivity(intent)
                finish()
            }else{
                Toast.makeText(baseContext, "por favor verifique su correo electronico",
                    Toast.LENGTH_SHORT).show()
            }
        }
        else
        {
            Toast.makeText(baseContext, "Bienvenido",
                Toast.LENGTH_SHORT).show()
        }
    }
    private fun updateUI(currentUser : FirebaseUser?){

        if (currentUser != null){
            if(currentUser.isEmailVerified) {
                dialog.dismiss()
                var intent = Intent(this,UserMenuActivity::class.java)
                intent.putExtra("email", currentUser.email)
                startActivity(intent)
                finish()
            }else{
                dialog.dismiss()
                Toast.makeText(baseContext, "por favor verifique su correo electronico",
                        Toast.LENGTH_SHORT).show()
            }


        }
        else
        {
            Toast.makeText(baseContext, "Bienvenido",
                    Toast.LENGTH_SHORT).show()
        }
    }

    private fun doLogin(){
        //Verificar correo
        if(!Patterns.EMAIL_ADDRESS.matcher(userText.text.toString()).matches() || userText.text.toString().isEmpty()){
            userText.error = "por favor ingrese su usuario con su correo en formato aaa@bbb.com"
            userText.requestFocus()
            return
        }
        //Verificar contraseña
        if(passwordText.text.toString().isEmpty()){
            passwordText.error = "el campo de usuario y/o contraseña no puede estar vacio"
            passwordText.requestFocus()
            return
        }
        progressDialog()
        auth.signInWithEmailAndPassword(userText.text.toString(), passwordText.text.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = auth.currentUser
                        updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user
                        dialog.dismiss()
                        if(notConnected){
                            Toast.makeText(baseContext, "Revisa tu conexión", Toast.LENGTH_SHORT).show()
                            updateUI(null)
                        }else{
                            Toast.makeText(baseContext, "Su usuario y/o contraseña son incorrectos", Toast.LENGTH_SHORT).show()
                            updateUI(null)
                        }


                        // ...
                    }

                }
    }

    private fun disconnected() {
        Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_LONG).show()
    }

    private fun connected() {
        Toast.makeText(baseContext, "Online", Toast.LENGTH_LONG).show()
        fetchFeeds()
    }

    private fun fetchFeeds() {
        retrofit.create(ApiService::class.java)
                .getFeeds()
                /*.enqueue(object: Callback {
                    override fun onFailure(call: Call<String>, t: Throwable) {
                        Log.e("MainActivityTag", t.message)
                    }

                    override fun onResponse(call: Call<String>?, response: Response<String>) {
                        addTitleToList(response.body()!!)
                    }

                })*/
    }
   /* private fun addTitleToList(response: String) {
        val jsonObject = JSONObject(response).getJSONObject("data")
        val children = jsonObject.getJSONArray("children")

        for (i in 0..(children.length()-1)) {
            val item = children.getJSONObject(i).getJSONObject("data").getString("title")
            arrayList.add(item)
            adapter.setItems(arrayList)
        }
    }*/

    fun logEvent(){
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image")
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)

        val paramsPrice = Bundle()
        paramsPrice.putInt("price", price)
        firebaseAnalytics.logEvent("registered_price", paramsPrice)

        val paramsCategory = Bundle()
        paramsCategory.putString("category", ProductsActivity.productCate)

        val paramsStore = Bundle()
        paramsStore.putString("nombre", StoreActivity.storeName)
        firebaseAnalytics.logEvent("store_products", paramsStore)
    }

}
