package com.example.unicentro_v1_ms6

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.unicentro_v1_ms6.ConnectionReceiver.ConnectionReceiver
import com.example.unicentro_v1_ms6.database.model.UserModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_user_menu.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

class UserMenuActivity : AppCompatActivity() {

    private lateinit var dbFirestore: FirebaseFirestore
    private lateinit var auth: FirebaseAuth

    var notConnected =true
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.reddit.com/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .client(OkHttpClient.Builder().build())
        .build()

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            notConnected = intent.getBooleanExtra(ConnectivityManager
                .EXTRA_NO_CONNECTIVITY, false)
            if (notConnected) {
                Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            } else {
                Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            }
        }
    }


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_menu)
        dbFirestore = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
        dbFirestore.collection("users").whereEqualTo("usuario", auth.currentUser?.email)
            .addSnapshotListener(this) { querySnapshot: QuerySnapshot?, e: FirebaseFirestoreException? ->
                if (e != null) {
                    Log.w("UserMenuActivity", "Listen failed.", e)
                    return@addSnapshotListener
                }
                if (querySnapshot != null) {
                    for (document in querySnapshot.documents) {
                        val userModel = document.toObject(UserModel::class.java)
                        textView.text="Bienvenido "+userModel?.nombre
                    }
                }
                else {
                    Toast.makeText(baseContext,"No hay productos",Toast.LENGTH_LONG).show()
                }
            }
    }



    public override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }


    /** Called when the user taps the buttons */
    fun sendMap(view: View) {
        val intent = Intent(this, GMapsActivity::class.java).apply {
        }
        startActivity(intent)
    }

    fun sendRoute(view: View) {
        val intent = Intent(this, RouteActivity::class.java).apply {
        }
        startActivity(intent)
    }

    fun sendProfile(view: View) {
        val intent = Intent(this, UserProfileActivity::class.java).apply {
        }
        startActivity(intent)
    }

    fun sendProducts(view: View) {
        val intent = Intent(this, GroceryActivity::class.java).apply {
        }
        startActivity(intent)
    }

    fun sendStores(view: View) {
        val intent = Intent(this, StoreActivity::class.java).apply {
        }
        startActivity(intent)
    }

    fun logOut(view: View){
        FirebaseAuth.getInstance().signOut()
        finish()
        val intent = Intent(this, MainActivity::class.java).apply {  }
        startActivity(intent)
    }
}
