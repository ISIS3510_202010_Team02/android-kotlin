package com.example.unicentro_v1_ms6

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.unicentro_v1_ms6.ConnectionReceiver.ConnectionReceiver
import com.example.unicentro_v1_ms6.Dialogs.DatePickerFragment
import com.example.unicentro_v1_ms6.database.model.UserModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_user_registration.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory


class UserRegistrationActivity : AppCompatActivity(),
    ConnectionReceiver.ConnectionReceiverListener, AdapterView.OnItemSelectedListener {

    //firebase
    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore

    //user fields
    private lateinit var nombre: EditText
    private lateinit var cedula: EditText
    private lateinit var ciudad: EditText
    private lateinit var usuario: EditText
    private lateinit var etBirthDate: EditText
    private lateinit var genre: String
    private lateinit var nation: String

    private lateinit var dialog: AlertDialog

    var notConnected = true
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.reddit.com/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .client(OkHttpClient.Builder().build())
        .build()

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            } else {
                Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            }
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (isConnected) {
            Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
        }
    }

    public override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_registration)

        nombre = findViewById(R.id.nameInput)
        cedula = findViewById(R.id.cedulaInput)
        ciudad = findViewById(R.id.cityInput)
        usuario = findViewById(R.id.userInput)
        etBirthDate = findViewById(R.id.etPlannedDate)

        //firebase
        db = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()

        etBirthDate.setOnClickListener {
            showDatePickerDialog()
        }

        button3.setOnClickListener {
            signUpUser()

        }

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.Genre_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinnerGenre.adapter = adapter
        }
        spinnerGenre.prompt="Genero"
        spinnerGenre.onItemSelectedListener = this

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.countries_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinnerNation.adapter = adapter
        }
        spinnerNation.prompt="Nacionalidad"
        spinnerNation.onItemSelectedListener = this
    }

    companion object {
        private val TAG = "UserRegistration"
    }


    private fun showDatePickerDialog() {
        val newFragment = DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener { _, year, month, day ->
            // +1 because January is zero
            val selectedDate = day.toString() + " / " + (month + 1) + " / " + year
            etBirthDate.setText(selectedDate)
        })

        newFragment.show(supportFragmentManager, "datePicker")
    }
    /** Sigun user method, called in onCreate for restrictions on text inputs*/
    private fun signUpUser() {
        //Verificar nombre
        if (nameInput.text.toString().isEmpty()) {
            nameInput.error = "por favor ingrese su nombre"
            nameInput.requestFocus()
            return
        }
        //Verificar cedula
        else if (cedulaInput.text.toString().isEmpty()) {
            cedulaInput.error = "el campo cédula no puede estar vacio"
            cedulaInput.requestFocus()
            return
        }
        //Verificar ciudad
        else if (cityInput.text.toString().isEmpty()) {
            cityInput.error = "el campo ciudad no puede estar vacio"
            cityInput.requestFocus()
            return
        }
        //Verificar ciudad
        else if (spinnerNation.getSelectedItem()=="Nacionalidad") {
            (spinnerNation.getSelectedView() as TextView).error = "Especifica tu nacionalidad"
            spinnerNation.requestFocus()
            return
        }
        //Verificar ciudad
        else if (spinnerGenre.getSelectedItem()=="Género") {
            (spinnerGenre.getSelectedView() as TextView).error = "Especifica tu género"
            spinnerGenre.requestFocus()
            return
        }
        //Verificar fecha
        else if (etBirthDate.text.toString().isEmpty()) {
            etBirthDate.error = "por favor ingrese su fecha de nacimiento"
            etBirthDate.requestFocus()
            return
        }
        //Verificar correo
        else if (!Patterns.EMAIL_ADDRESS.matcher(userInput.text.toString()).matches()) {
            etBirthDate.error=null
            userInput.error = "por favor ingrese su usuario con su correo en formato aaa@bbb.com"
            userInput.requestFocus()
            return
        }

        //Verificar contraseña
        else if (passWordInput.text.toString().isEmpty()) {
            passWordInput.error = "el campo de contraseña no puede estar vacio"
            passWordInput.requestFocus()
            return
        }
        progressDialog()
        auth.createUserWithEmailAndPassword(userInput.text.toString(),passWordInput.text.toString())
            .addOnSuccessListener(this) { task ->
                val user = auth.currentUser
                user?.sendEmailVerification()
                    ?.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val user2 = UserModel(
                                nameInput.text.toString(),
                                cedulaInput.text.toString(),
                                cityInput.text.toString(),
                                userInput.text.toString(),
                                spinnerNation.selectedItem.toString(),
                                spinnerGenre.selectedItem.toString(),
                                etBirthDate.text.toString()
                            )
                            addUserFirestore(user2)
                        } else {
                            dialog.dismiss()
                            Toast.makeText(baseContext, "Algo pasó, pero no sabemos el qué bro.",Toast.LENGTH_SHORT).show()
                        }
                    }
            }
            .addOnFailureListener(this) { task ->
                dialog.dismiss()
                val err = task.message.toString()
                if ("email" in err) {
                    Toast.makeText(baseContext, "El correo ya se encuentra en uso por otro usuario.",Toast.LENGTH_SHORT).show()
                } else if ("internal" in err) {
                    Toast.makeText(baseContext, "Reestablece la conexión para registrarte.",Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(baseContext, err,Toast.LENGTH_SHORT).show()
                }
            }

    }

    /** Called when the user taps the buttons */
    fun sendProfile(view: View) {
        val intent = Intent(this, UserProfileActivity::class.java).apply {
        }
        startActivity(intent)
    }

    @SuppressLint("SetTextI18n")
    fun progressDialog() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog, null)
        val message = dialogView.findViewById<TextView>(R.id.message)
        message.text = "Registrando usuario..."
        builder.setView(dialogView)
        builder.setCancelable(false)
        dialog = builder.create()
        dialog.show()
    }

    private fun addUserFirestore(user: UserModel) {
        db.collection("users")
            .add(user)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                dialog.dismiss()
                finish()
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error:  ", e)
                dialog.dismiss()
                Toast.makeText(
                    baseContext,
                    "No fue posible crear tu usuario, intenta mas tarde",
                    Toast.LENGTH_LONG
                ).show()
            }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val choice = parent?.getItemAtPosition(position).toString()
        val spin = parent as Spinner
        if(spin.id==R.id.spinnerGenre){
            if(choice!="género"){
                genre=choice;
            }
        }
        else if(spin.id==R.id.spinnerNation){
            if(choice!="Nacionalidad"){
                nation=choice;
            }
        }

    }
}
