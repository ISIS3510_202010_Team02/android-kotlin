package com.example.unicentro_v1_ms6

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.example.unicentro_v1_ms6.database.model.StoreModel
import javax.annotation.Nullable
import javax.sql.DataSource


class StoreAdapter(val mCtx: Context, val layoutResId: Int, val storeList: List<StoreModel>):
ArrayAdapter<StoreModel>(mCtx,layoutResId,storeList) {



    @SuppressLint("ResourceAsColor")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(layoutResId,null)
        val textViewName = view.findViewById<TextView>(R.id.textViewProducts)
        val textViewSchedule = view.findViewById<TextView>(R.id.price)
        val imagenStore = view.findViewById<ImageView>(R.id.imageProduct)
        val progressBar = view.findViewById<ProgressBar>(R.id.homeprogress)
        var imageMore: TextView = view.findViewById(R.id.imageMore)
        imageMore.visibility=View.INVISIBLE
        val store = storeList[position]
        textViewName.text=store.name

        textViewSchedule.text="Horario: "+store.schedule

        Glide.with(mCtx)
            .load(store.img)
            .listener(object : RequestListener<Drawable?> {
                override fun onLoadFailed(
                    @Nullable e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable?>?,
                    isFirstResource: Boolean
                ): Boolean {
                    progressBar.setVisibility(View.GONE)
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable?>?,
                    dataSource: com.bumptech.glide.load.DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    if(textViewName.text.equals("Carulla")){
                        imagenStore.setBackgroundColor(Color.parseColor("#8dc63f"))
                    }
                    progressBar.setVisibility(View.GONE)
                    return false
                }
            }).thumbnail(0.1f)
            .into(imagenStore)
        return  view
    }




}