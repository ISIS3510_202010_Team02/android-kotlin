package com.example.unicentro_v1_ms6

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import com.example.unicentro_v1_ms6.ConnectionReceiver.ConnectionReceiver
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.firebase.database.core.Tag
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*

class GMapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener, ConnectionReceiver.ConnectionReceiverListener {

    //Widgets
    private lateinit var mSearchText: EditText

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var lastLocation: Location

    private lateinit var mMap: GoogleMap

    private lateinit var addressLatLng: LatLng

    var notConnected =true
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.reddit.com/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .client(OkHttpClient.Builder().build())
        .build()

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            notConnected = intent.getBooleanExtra(
                ConnectivityManager
                .EXTRA_NO_CONNECTIVITY, false)
            if (notConnected) {
                Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            } else {
                Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            }
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean){
        if(isConnected){
            Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gmaps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.


        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

//        mSearchText = findViewById(R.id.input_search)



//        init()


        // Initialize the AutocompleteSupportFragment.
/**
        // Initialize the AutocompleteSupportFragment.
        val autocompleteFragment: AutocompleteSupportFragment? =
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment?

// Specify the types of place data to return.

// Specify the types of place data to return.
        if (autocompleteFragment != null) {
            autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME))
        }

// Set up a PlaceSelectionListener to handle the response.

// Set up a PlaceSelectionListener to handle the response.
        if (autocompleteFragment != null) {
            autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
                override fun onPlaceSelected(place: Place) {
                    // TODO: Get info about the selected place.
                    Log.i(
                        TAG,
                        "Place: " + place.getName().toString() + ", " + place.getId()
                    )
                }

                override fun onError(status: Status) {
                    // TODO: Handle the error.
                    Log.i(TAG, "An error occurred: $status")
                }
            })
        }
        */
        onNetworkConnectionChanged(MainActivity.not_connected)
    }

    companion object{
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private val TAG = "GMapsActivity"
    }

    public override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    /*private fun init(){
        Log.d("init: intializing map", "init")

        mSearchText.setOnEditorActionListener(TextView.OnEditorActionListener(
             fun (textView: TextView, actionID:Int, keyEvent:KeyEvent):Boolean{
                if(actionID == EditorInfo.IME_ACTION_SEARCH
                    || actionID == EditorInfo.IME_ACTION_DONE
                    || keyEvent.action == KeyEvent.ACTION_DOWN
                    || keyEvent.action == KeyEvent.KEYCODE_ENTER){
                    geoLocate()
                }

                return false
            }
        ))
    }

    fun geoLocate(){
        Log.d("Geolocate: geolocation","init")
        var searchString = mSearchText.text.toString()

        var geoCoder = Geocoder(this)
        var list = ArrayList<Address>()
        try {
            list = geoCoder.getFromLocationName(searchString, 1) as ArrayList<Address>
        }catch (e:IOException){
            Log.d("Geolocate IOException", e.message)
        }

        if(list.size > 0){
            var address = list.get(0)
            Log.d("found", "found a location" + address.toString())
            //Toast.makeText(this, address.toString(), Toast.LENGTH_SHORT).show()

            addressLatLng = LatLng(address.latitude,address.longitude)


        }
    }*/

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        /*val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))*/

        mMap.setOnMarkerClickListener(this)
        mMap.uiSettings.isZoomControlsEnabled = true

        setUpMap()

    }

    private fun placeMarker(location: LatLng){

        val markerOption = MarkerOptions().position(location)

        mMap.addMarker(markerOption)

    }
    private fun placeCustomMarker(){
        var esquina1 = mMap.addMarker(MarkerOptions().position(LatLng(4.705067, -74.036123)).title("tienda esquina 1").snippet("usuarios cerca: 1"))
        var exito134 = mMap.addMarker(MarkerOptions().position(LatLng(4.712145, -74.034187)).title("Exito 134").snippet("usuarios cerca: 2"))
        var carullaExpress = mMap.addMarker(MarkerOptions().position(LatLng(4.704455, -74.040120)).title("Carulla Express 127").snippet("usuarios cerca: 1"))
        var exitoUni = mMap.addMarker(MarkerOptions().position(LatLng(4.703351, -74.041965)).title("Exito unicentro").snippet("usuarios cerca: 1"))
        var carulla2 = mMap.addMarker(MarkerOptions().position(LatLng(4.6185395, -74.104923)).title("Carulla").snippet("usuarios cerca: 0"))
        var carulla3 = mMap.addMarker(MarkerOptions().position(LatLng(4.6196174, -74.1002652)).title("Carulla").snippet("usuarios cerca: 0"))
        var carulla4 = mMap.addMarker(MarkerOptions().position(LatLng(4.6069448, -74.1004668)).title("Carulla").snippet("usuarios cerca: 0"))
        var carulla5 = mMap.addMarker(MarkerOptions().position(LatLng(4.6196174, -74.1002652)).title("Carulla").snippet("usuarios cerca: 0"))

        esquina1.showInfoWindow()
        exito134.showInfoWindow()
        carullaExpress.showInfoWindow()
        exitoUni.showInfoWindow()
    }

    private fun setUpMap(){
        if(ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
                return
        }



        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(this) {location ->

            if(location != null){
                lastLocation = location
                val currentLatLong = LatLng(location.latitude,location.longitude)
                placeMarker(currentLatLong)
                placeCustomMarker()
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 16f))
            }
        }
    }




    override fun onMarkerClick(p0: Marker?): Boolean {
        return false
    }
}
