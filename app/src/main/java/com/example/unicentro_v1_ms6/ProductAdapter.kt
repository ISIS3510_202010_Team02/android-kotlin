package com.example.unicentro_v1_ms6

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.example.unicentro_v1_ms6.database.model.ProductModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import javax.annotation.Nullable

class ProductAdapter(private val mCtx: Context, private val layoutResId: Int, private val productList: List<ProductModel>):
    ArrayAdapter<ProductModel>(mCtx,layoutResId,productList) {

    private var yourActivity: ProductsActivity? = null

    fun yourArrayAdapter(yourActivity: ProductsActivity?) {
        this.yourActivity = yourActivity
    }

    private var shopActivity: GroceryActivity? = null

    fun groceryAdapter(yourActivity: GroceryActivity?) {
        this.shopActivity = yourActivity
    }

    private  var auth: FirebaseAuth = FirebaseAuth.getInstance()
    //declaration of Firestore database
    private  var db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val dbRefPrice = db.collection(auth.currentUser?.uid.toString()+1).document("total")

     fun total(){
        var sum=0
        for( product in productList){
            sum+= product.price!!* product.amount!!
        }
        dbRefPrice.set(mapOf("price" to sum))
    }


    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(layoutResId,null)
        val textViewName = view.findViewById<TextView>(R.id.textViewProducts)
        val textViewPrice = view.findViewById<TextView>(R.id.price)
        val imageProduct = view.findViewById<ImageView>(R.id.imageProduct)
        val progressBar = view.findViewById<ProgressBar>(R.id.homeprogress)
        var addButton: Button = view.findViewById(R.id.IdAgregar)
        var plusButton: Button = view.findViewById(R.id.addButton)
        var minButton: Button = view.findViewById(R.id.minButton)
        var starButton: TextView = view.findViewById(R.id.imageMore)
        var stockText: TextView = view.findViewById(R.id.stockText)


        val product = productList[position]
        val dbRef = db.collection(auth.currentUser?.uid.toString()).document(product.name.toString()+" "+product.store.toString())


        val amount = product.amount.toString()
        if(amount.toInt() > 0) {
            plusButton.visibility=View.VISIBLE
            minButton.visibility=View.VISIBLE
            stockText.visibility=View.VISIBLE

        }else{
            addButton.visibility=View.VISIBLE

        }

        stockText.text=product.amount.toString()
        textViewName.text=product.name
        textViewPrice.text="$ ${product.price}"
        Glide.with(mCtx)
            .load(product.img)
            .listener(object : RequestListener<Drawable?> {
                override fun onLoadFailed(
                    @Nullable e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable?>?,
                    isFirstResource: Boolean
                ): Boolean {
                    progressBar.visibility = View.GONE
                    return false
                }
                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable?>?,
                    dataSource: com.bumptech.glide.load.DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progressBar.visibility = View.GONE
                    return false
                }
            }).thumbnail(0.1f)
            .into(imageProduct)

        starButton.setOnClickListener {
            shopActivity?.setMarks(db.collection("productos2").document(product.name.toString()+" "+product.store.toString()));
            yourActivity?.setMarks(db.collection("productos2").document(product.name.toString()+" "+product.store.toString()));

        }
        addButton.setOnClickListener {

            addButton.visibility=View.INVISIBLE
            plusButton.visibility=View.VISIBLE
            minButton.visibility=View.VISIBLE
            stockText.visibility=View.VISIBLE
            Toast.makeText(mCtx,product.name.toString(),Toast.LENGTH_LONG).show()
            product.amount = product.amount!! +1
            stockText.text=product.amount.toString()
            dbRef.set(product)
            dbRefPrice.update("price", FieldValue.increment(product.price?.toLong()!!))

        }

        plusButton.setOnClickListener {
            if(product.amount!! >=15){
                Toast.makeText(mCtx,"Máximo 15 unidades. No Acapares",Toast.LENGTH_LONG).show()
            }
            else{
                product.amount = product.amount!! +1
                stockText.text= (product.amount).toString()
                dbRef.update("amount", product.amount)
                dbRefPrice.update("price", FieldValue.increment(product.price?.toLong()!!))
            }


        }
        minButton.setOnClickListener {
            product.amount = product.amount!! -1
            dbRefPrice.update("price", FieldValue.increment(-product.price?.toLong()!!))
            if(product.amount!! <=0) {
                addButton.visibility = View.VISIBLE
                plusButton.visibility = View.INVISIBLE
                minButton.visibility = View.INVISIBLE
                stockText.visibility = View.INVISIBLE
                dbRef.delete()
            }
            else{
                dbRef.update("amount", product.amount)
            }
            stockText.text= (product.amount).toString()



        }
        return  view
    }



}