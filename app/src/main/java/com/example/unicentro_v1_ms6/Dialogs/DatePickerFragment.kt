package com.example.unicentro_v1_ms6.Dialogs

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import com.example.unicentro_v1_ms6.UserRegistrationActivity
import java.util.*
import com.example.unicentro_v1_ms6.UserRegistrationActivity.Companion as UserRegistrationActivity1

class DatePickerFragment : DialogFragment(){


    private var listener: DatePickerDialog.OnDateSetListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog =DatePickerDialog(this.context!!, listener, year - 18, month, day)
        // Min and max date
        c.set(Calendar.YEAR, year - 100)
        datePickerDialog.datePicker.minDate = c.timeInMillis
        c.set(Calendar.YEAR, year - 10)
        datePickerDialog.datePicker.maxDate = c.timeInMillis

        return datePickerDialog;
    }

    companion object {
        fun newInstance(listener: DatePickerDialog.OnDateSetListener): DatePickerFragment {
            val fragment = DatePickerFragment()
            fragment.listener = listener
            return fragment
        }
    }

}

