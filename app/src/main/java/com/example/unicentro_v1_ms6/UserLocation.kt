package com.example.unicentro_v1_ms6

import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ServerTimestamp
import com.google.firebase.firestore.auth.User

class UserLocation {
    private lateinit var geo_point: GeoPoint
    @ServerTimestamp
    private lateinit var timestamp: String
    private lateinit var user: User

    fun onCreate(){
        this.geo_point = geo_point
        this.timestamp = timestamp
        this.user = user
    }

    fun setUser(user: User){
        this.user = user
    }
    fun getUser(): User{
        return user
    }

    fun setGeoPoint(geo_point: GeoPoint){
        this.geo_point = geo_point
    }
    fun getGeoPoint():GeoPoint{
        return geo_point
    }

    fun setTimestamp(){
        this.timestamp = timestamp
    }
    fun getTimestamp(): String{
        return timestamp
    }
}