package com.example.unicentro_v1_ms6

import android.annotation.SuppressLint
import android.content.*
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.unicentro_v1_ms6.ConnectionReceiver.ConnectionReceiver
import com.example.unicentro_v1_ms6.database.model.UserModel
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_user_profile.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory


class UserProfileActivity : AppCompatActivity(), ConnectionReceiver.ConnectionReceiverListener {


    private lateinit var auth: FirebaseAuth
    private lateinit var dbFirestore: FirebaseFirestore
    private lateinit var userList: MutableList<UserModel>

    //progress dialog
    private lateinit var dialog: android.app.AlertDialog

    var notConnected = true
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.reddit.com/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .client(OkHttpClient.Builder().build())
        .build()

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            } else {
                Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
                MainActivity.not_connected = notConnected
            }
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (isConnected) {
            Toast.makeText(baseContext, "Estas sin conexión", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(baseContext, "online", Toast.LENGTH_SHORT).show()
        }
    }

    public override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        //listView.findViewById<ListView>(R.id.productList)
        userList = mutableListOf()
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
        dbFirestore = FirebaseFirestore.getInstance()

        dbFirestore.collection("users").whereEqualTo("usuario", auth.currentUser?.email)
            .addSnapshotListener(
                this
            ) { querySnapshot: QuerySnapshot?, e: FirebaseFirestoreException? ->
                if (e != null) {
                    Log.w(UserProfileActivity.TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }
                if (querySnapshot != null) {
                    for (document in querySnapshot.documents) {
                        val userModel = document.toObject(UserModel::class.java)
                        Log.d(TAG, "${document.id} => ${document.data}")
                        textView6.text = "Bienvenido " + userModel?.nombre
                        nombreText.text = userModel?.nombre
                        cedulaText.text = userModel?.cedula
                        ciudadText.text = userModel?.ciudad
                        usuarioText.text = userModel?.usuario
                    }
                } else {
                    Toast.makeText(
                        baseContext,
                        "No es posible mostrar los datos del perfil en este momento",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
    }

    companion object {

        private val TAG = "UserActivity"

    }

    fun logOut(view: View) {
        FirebaseAuth.getInstance().signOut()
        finish()
        val intent = Intent(this, MainActivity::class.java).apply { }
        startActivity(intent)
    }



    fun delete(view: View) {
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        val filterArray = arrayOfNulls<InputFilter>(1)
        filterArray[0] = LengthFilter(15)
        input.filters=filterArray

        val dialog: AlertDialog =
            AlertDialog.Builder(this)
                .setView(input)
                .setTitle("Eliminar cuenta")
                .setMessage("Digita tu contraseña para confirmar: ")
                .setPositiveButton("Enviar", null) //Set to null. We override the onclick
                .setNegativeButton("Cancelar", null)
                .create()

        dialog.setOnShowListener {
            val button: Button =
                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener(View.OnClickListener { // TODO Do something
                if(input.text.isNotEmpty()){
                    progressDialog()
                    deleteAccount(input.text.toString())
                    dialog.dismiss()
                } else{
                    input.error = "el campo de contraseña no puede estar vacío"
                    input.requestFocus()
                }
            })
        }
        dialog.show()
    }

    private fun deleteAccount(password:String){
        val user = FirebaseAuth.getInstance().currentUser
        val credential = EmailAuthProvider
            .getCredential(user?.email.toString(), password)

        user?.reauthenticate(credential)
            ?.addOnSuccessListener {
                del(user)
            }
            ?.addOnFailureListener{task ->
                dialog.dismiss()
                when {
                    "internal" in task.message.toString() -> {
                        Toast.makeText(baseContext,"Reestablece la conexión para eliminar la cuenta",Toast.LENGTH_LONG).show()
                    }
                    "password" in task.message.toString() -> {
                        Toast.makeText(baseContext,"Contraseña de usuario incorrecta",Toast.LENGTH_LONG).show()
                    }
                    else -> {
                        Toast.makeText(baseContext,task.message,Toast.LENGTH_LONG).show()
                    }
                }

            }

    }
    private fun del(user :FirebaseUser ){
        user.delete()
            .addOnSuccessListener {
                dialog.dismiss()
                Toast.makeText(baseContext,"Se eliminó exitosamente la cuenta",Toast.LENGTH_LONG).show()
                finish()
                val intent = Intent(this, MainActivity::class.java).apply { }
                startActivity(intent)
            }
            .addOnFailureListener { task ->
                dialog.dismiss()
                when {
                    "internal" in task.message.toString() -> {
                        Toast.makeText(baseContext,"Reestablece la conexión para eliminar la cuenta",Toast.LENGTH_LONG).show()
                    }
                    else -> {
                        Toast.makeText(baseContext,task.message,Toast.LENGTH_LONG).show()
                    }
                }
            }
    }

    private fun progressDialog(){
        val builder = android.app.AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        val message = dialogView.findViewById<TextView>(R.id.message)
        message.text="Eliminando usuario..."
        builder.setView(dialogView)
        builder.setCancelable(false)
        dialog= builder.create()
        dialog.show()
    }
}

